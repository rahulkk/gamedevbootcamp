﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {
    public GameObject sentry;
    public Transform[] spawnPositions;
	public float spawnDelay = 1.5f;

	// Use this for initialization
	void Start () {
		// Task 2: Start Your Coroutine Here
		StartCoroutine(SpawnEnemies());
	}
	
	// Update is called once per frame
	void Update () {
		// Don't use Update for this task!
	}

    // Task 2: Write Your Coroutine Here
	private IEnumerator SpawnEnemies() {
		int i = 0;
		while (true) {
			Instantiate(sentry, spawnPositions[i].position, spawnPositions[i].rotation);
			i++;

			if (i >= spawnPositions.Length) {
				i = 0;
			}

			yield return new WaitForSeconds(spawnDelay);
		}
	}
}
