﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {
	public float moveSpeed = 2f;

	private Coroutine movement;

	// Use this for initialization
	void Start() {
	}

	// Update is called once per frame
	void Update() {
		// Don't use Update for this task
	}

	void OnTriggerEnter2D(Collider2D collision) {
		// Task 3: Start Your Coroutine Here
		if (movement != null) {
			StopCoroutine(movement);
		}
		movement = StartCoroutine(MoveTo(collision.transform.position));
	}

	// Task 3: Write Your Coroutine Here
	private IEnumerator MoveTo(Vector3 position) {
		float elapsedTime = 0f;
		float totalTime = (position - transform.position).magnitude / moveSpeed;
		Vector3 startPos = transform.position;

		while (elapsedTime < totalTime) {
			transform.position = Vector3.Lerp(startPos, position, elapsedTime / totalTime);
			elapsedTime += Time.deltaTime;
			yield return null;
		}
	}
}
