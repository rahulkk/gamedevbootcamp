﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class PlayerController : MonoBehaviour {
    public Rigidbody2D playerRigidbody;

	private float deathTime = 1.5f;

	// Use this for initialization
	void Start () {
        playerRigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        playerRigidbody.velocity = movement * 2;

        if(Input.GetKeyDown(KeyCode.F)) {
			// Task 1: Start Your Coroutine Here
			StartCoroutine(Die());
        }
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Wall") {
            playerRigidbody.velocity = Vector2.zero;
        }
    }

    // Task 1: Write Your Coroutine Here

	private IEnumerator Die() {
		float elapsedTime = 0f;
		SpriteRenderer sprite = GetComponent<SpriteRenderer>();
		Color startColor = sprite.color;

		while (elapsedTime < deathTime) {
			sprite.color = Color.Lerp(startColor, Color.black, elapsedTime / deathTime);
			elapsedTime += Time.deltaTime;
			yield return null;
		}
	}

}