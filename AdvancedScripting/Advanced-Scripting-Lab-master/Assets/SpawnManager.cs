﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {
	public List<Enemy> enemies;
	public float spawnDelay = 0.25f;

	private float timeElapsed = 0f;

	private void Update() {
		timeElapsed += Time.deltaTime;

		if (timeElapsed > spawnDelay) {
			timeElapsed = 0f;
			SpawnEnemy();
		}
	}

	private void SpawnEnemy() {
		int index = Random.Range(0, enemies.Count);
		Instantiate(enemies[index]);
	}
}
