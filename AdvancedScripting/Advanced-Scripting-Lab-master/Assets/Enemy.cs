﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Enemy : MonoBehaviour {
	public bool good = true;

	private Vector3 start;
	private float time = 0f;
	private float radius = 1f;
	private float speed = 2f;

	// Use this for initialization
	void Start () {
		start = new Vector3(Random.Range(-5, 5), Random.Range(-3, 3), 0);
		speed = Random.Range(2f, 8f);
		transform.position = start;
		radius = Random.Range(1f, 4.5f);
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		transform.position = start + new Vector3(radius * Mathf.Cos(time), radius * Mathf.Sin(time));
	}
}
